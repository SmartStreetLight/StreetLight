using System;
using System.ComponentModel;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Assets.Pong
{
    public class Ball : MonoBehaviour
    {
        public float speed = 5f;

        public event EventHandler<Side> GoalScored;
        bool scored;

        public void Recenter()
        {
            var component = GetComponent<Rigidbody>();
            component.velocity = Vector3.zero;
            component.transform.position = Vector3.zero;
            scored = false;
        }

        public void Kickoff()
        {
            float sx = Random.Range(0, 2) == 0 ? -1 : 1;
            float sz = Random.Range(0, 2) == 0 ? -1 : 1;

            GetComponent<Rigidbody>().velocity = new Vector3(speed * sx, 0f, speed * sz);
        }

        private void Update()
        {
            var xPosition = GetComponent<Rigidbody>().transform.position.x;
            if (xPosition > 15 && !scored)
            {
                scored = true;
                GoalScored.Invoke(this, Side.Right);
            }
            else if (xPosition < -15 && !scored)
            {
                scored = true;
                GoalScored.Invoke(this, Side.Left);
            }
        }
    }
}