﻿using Assets.StreetLight.Interfaces;
using Assets.StreetLight.Poco;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.StreetLight.Adapters
{
    internal class ZedPersonDetector : IPersonDetector
    {
        private readonly ZEDManager zedManager;

        public event EventHandler<IEnumerable<Person>> PersonsDetected;

        public ZedPersonDetector(ZEDManager zedManager)
        {
            this.zedManager = zedManager;
            zedManager.OnObjectDetection += ZedManager_OnObjectDetection;
            zedManager.OnZEDReady += ZedManager_OnZEDReady;
        }

        private void ZedManager_OnZEDReady()
        {
            zedManager.StartObjectDetection();
        }

        private void ZedManager_OnObjectDetection(DetectionFrame detectionFrame)
        {
            var detectedObjects = detectionFrame.GetFilteredObjectList(true, true, false);

            var persons = detectedObjects.Where(d => d.objectClass == sl.OBJECT_CLASS.PERSON);

            var detectedPersons = from p in persons
                                  let position = p.Get3DWorldPosition()
                                  select new Person(p.id, null)
                                  {
                                      WorldPosition = new Vector3(position.x, position.y, position.z)
                                  };

            PersonsDetected?.Invoke(this, detectedPersons.ToList());
        }
    }
}
