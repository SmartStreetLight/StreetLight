using Assets.StreetLight.Scripts;
using System;
using System.Collections.Generic;
using UnityEngine;

public class CalibrationMarkerBehavior : MonoBehaviour
{
    public float speed = 0.05f;

    Vector3[] frustumCorners;
    List<Vector3> targetPositions;
    PersonManager PersonManager => personManagerLazy.Value;
    Lazy<PersonManager> personManagerLazy;

    public event EventHandler PathFinished;

    private void Awake()
    {
        personManagerLazy = new Lazy<PersonManager>(FindObjectOfType<PersonManager>);
    }

    // Start is called before the first frame update
    void Start()
    {
        var camera = Camera.main;
        frustumCorners = new Vector3[4];
        camera.CalculateFrustumCorners(camera.rect, camera.transform.position.y, camera.stereoActiveEye, frustumCorners);

        targetPositions = new List<Vector3>()
        {
            new Vector3(frustumCorners[0].x, 0, frustumCorners[0].y),
            new Vector3(frustumCorners[1].x, 0, frustumCorners[1].y),
            new Vector3(frustumCorners[2].x, 0, frustumCorners[2].y),
            new Vector3(frustumCorners[3].x, 0, frustumCorners[3].y),
        };

        int count = 1;
        const int increment = 2;
        while (count < 4)
        {
            targetPositions.Add(new Vector3(frustumCorners[0].x + count * increment, 0, frustumCorners[0].y + (count - 1) * increment));
            targetPositions.Add(new Vector3(frustumCorners[1].x + count * increment, 0, frustumCorners[1].y - count * increment));
            targetPositions.Add(new Vector3(frustumCorners[2].x - count * increment, 0, frustumCorners[2].y - count * increment));
            targetPositions.Add(new Vector3(frustumCorners[3].x - count * increment, 0, frustumCorners[3].y + count * increment));
            count += 1;
        }

        currentTarget = GameObject.Find("CalibrationMarker").transform.position;

        targetPositions.Add(currentTarget);

        enabled = false;
        PersonManager.DetectionReady += PersonManager_DetectionReady;
    }

    private void PersonManager_DetectionReady(object sender, EventArgs e)
    {
        enabled = true;
    }

    int targetIndex = -1;
    Vector3 currentTarget = Vector3.zero;
    Vector3 normalizedTranslationVector = Vector3.zero;

    // Update is called once per frame
    void Update()
    {
        var marker = GameObject.Find("CalibrationMarker");

        if (Vector3.Distance(marker.transform.position, currentTarget) >= 0.05f)
        {
            marker.transform.position += Vector3.ClampMagnitude(speed * Time.deltaTime * normalizedTranslationVector, (currentTarget - marker.transform.position).magnitude);
        }
        else if (targetIndex < targetPositions.Count - 1)
        {
            targetIndex++;
            currentTarget = targetPositions[targetIndex];
            normalizedTranslationVector = (currentTarget - marker.transform.position).normalized;
        }
        else
        {
            enabled = false;
            PathFinished?.Invoke(this, EventArgs.Empty);
        }

        Color color = Color.Lerp(Color.green, Color.red, Time.deltaTime / 0.2f);
        Debug.Log(string.Format("<color=#{0:X2}{1:X2}{2:X2}>{3}</color>", (byte)(color.r * 255f), (byte)(color.g * 255f), (byte)(color.b * 255f), $"deltaTime: {Time.deltaTime}"));
    }
}
