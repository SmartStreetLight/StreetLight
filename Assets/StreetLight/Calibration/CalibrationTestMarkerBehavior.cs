using Assets.StreetLight.Scripts;
using System;
using System.Linq;
using UnityEngine;

public class CalibrationTestMarkerBehavior : MonoBehaviour
{
    PersonManager PersonManager => personManagerLazy.Value;
    Lazy<PersonManager> personManagerLazy;

    private void Awake()
    {
        personManagerLazy = new Lazy<PersonManager>(FindObjectOfType<PersonManager>);
    }


    void Update()
    {
        var firstPersonFound = PersonManager.Persons.FirstOrDefault();
        if (firstPersonFound != null)
        {
            var marker = GameObject.Find("CalibrationTestMarker");
            marker.transform.position = firstPersonFound.GetUnityPosition();
        }
    }
}
