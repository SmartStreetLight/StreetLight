﻿using Newtonsoft.Json;
using System;
using System.IO;
using UnityEngine;

namespace Assets.StreetLight
{
    public sealed class Configuration
    {
        private string homographyFilePath;
        /// <summary>
        /// The file path to the csv file containing the homography for mapping world coodrinates to Unity coordinates.
        /// </summary>
        [JsonRequired]
        public string HomographyFilePath
        {
            get => homographyFilePath;
            set
            {
                if (!File.Exists(value))
                {
                    throw new InvalidOperationException($"File \"{value}\" does not exist.");
                }
                homographyFilePath = value;
            }
        }

        private static readonly Lazy<Configuration> lazy = new(() =>
        {
            var dataPath = new DirectoryInfo(Application.streamingAssetsPath).FullName;
            var configFilePath = Path.Combine(dataPath, "StreetLight", "Configuration.json");
            return JsonConvert.DeserializeObject<Configuration>(File.ReadAllText(configFilePath));
        });

        public static Configuration Instance => lazy.Value;

        private Configuration() { }
    }
}
