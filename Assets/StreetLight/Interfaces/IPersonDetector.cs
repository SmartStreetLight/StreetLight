﻿using Assets.StreetLight.Poco;
using System;
using System.Collections.Generic;

namespace Assets.StreetLight.Interfaces
{
    public interface IPersonDetector
    {
        public event EventHandler<IEnumerable<Person>> PersonsDetected;
    }
}
