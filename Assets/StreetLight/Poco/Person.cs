﻿using Assets.StreetLight.Scripts;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace Assets.StreetLight.Poco
{
    public class Person : INotifyPropertyChanged
    {
        public int Id { get; }
        public event PropertyChangedEventHandler PropertyChanged;

        private Vector3 worldPosition = Vector3.zero;
        private readonly PositionCalculator positionCalculator;

        public Vector3 WorldPosition
        {
            get => worldPosition;
            set => OnPropertyChanged(ref worldPosition, value);
        }

        public Person(int id, PositionCalculator positionCalculator)
        {
            Id = id;
            this.positionCalculator = positionCalculator;
        }

        public Vector3 GetUnityPosition()
        {
            if (positionCalculator == null)
            {
                throw new InvalidOperationException("Cannot calculate Unity position because the PositionCalculator is not initialized. This might occur when the homography file is not found or not in the right format.");
            }

            return positionCalculator.WorldPositionToUnityPosition(WorldPosition);
        }


        private void OnPropertyChanged<T>(ref T field, T value, [CallerMemberName] string propertyName = null)
        {
            if (!field.Equals(value))
            {
                field = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
