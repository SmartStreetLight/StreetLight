﻿using sl;

public interface IZEDManager
{
    /// <summary>
    /// Whether the object detection module has been activated successfully.
    /// </summary>
    bool IsObjectDetectionRunning { get; }

    bool EstimateInitialPosition { get; }
    DETECTION_MODEL ObjectDetectionModel { get; }

    /// <summary>
    /// Event that's called whenever the Object Detection module detects a new frame.
    /// Supplies data in the form of a DetectionFrame instance, which has many helper functions for use in Unity.
    /// </summary>
    event ZEDManager.onNewDetectionTriggerDelegate OnObjectDetection;

    /// <summary>
    /// Called when the ZED has finished initializing successfully.
    /// Used by many scripts to run startup logic that requires that the ZED is active.
    /// </summary>
    event ZEDManager.OnZEDManagerReady OnZEDReady;

    void StartObjectDetection();
}
